import React from 'react'

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      dateTime: new Date().toLocaleTimeString(),
    };
    console.log(1);
  }

  show() {
    console.log();
    setInterval(() => {
      this.setState({
        dateTime: new Date().toLocaleTimeString(),
      })
    }, 5000);
  }

  componentDidMount() {
    console.log(3);
    this.show();
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    console.log(4)
  }

  componentWillUnmount() {
  }

  render() {
    console.log(2)
    return (
      <div>
        {this.state.dateTime}
      </div>
    )
  }
}

export default App;
